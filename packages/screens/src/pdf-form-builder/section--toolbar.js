import React from 'react'
import {useStore} from 'effector-react'

import {fromEditor} from './domain--editor'

import html2canvas from 'html2canvas'
import pdfMake from 'pdfmake/build/pdfmake'

const t = {
  add: `Add field`,
  addSign: `Add sign field`,
  preview: `Switch to editor`,
  editor: `Switch to preview`,
}

const defaults = {
  fileName: `export.pdf`,
  width: 500,
}

async function onSave() {
  try {
    console.log(`SectionToolbar`, `onSave`)
    const canvas = await html2canvas(document.getElementById('pdf'))
    const data = canvas.toDataURL()
    const options = {
      content: [
        {
          image: data,
          width: defaults.width,
        },
      ],
    }
    pdfMake.createPdf(options).download(defaults.fileName)
  } catch (err) {
    console.log(`SectionToolbar`, `onSave`, err)
  }
}

export function SectionToolbar() {
  const isPreview = useStore(fromEditor.$.isPreview)

  return (
    <>
      <button onClick={fromEditor.run.add}>{t.add}</button>
      <button onClick={fromEditor.run.addSign}>{t.addSign}</button>
      <button onClick={fromEditor.on.toggle}>
        {isPreview ? t.preview : t.editor}
      </button>
      <button onClick={onSave}>Save</button>
    </>
  )
}
