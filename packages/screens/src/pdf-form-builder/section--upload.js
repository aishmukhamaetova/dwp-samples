import React from 'react'
import {createEffect} from 'effector'

import {fromEditor} from './domain--editor'

const on = {}
on.change = fromEditor.on.file.prepend((e) => e.target.files[0])

export function SectionUpload() {
  return <input type="file" onChange={on.change} />
}
