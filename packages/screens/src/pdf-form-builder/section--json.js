import React from 'react'
import {useStore} from 'effector-react'

import {fromFields} from './domain--fields'

const $text = fromFields.$.data.map((state) => JSON.stringify(state, null, 2))

export function SectionJson() {
  const text = useStore($text)

  return <pre>{text}</pre>
}
