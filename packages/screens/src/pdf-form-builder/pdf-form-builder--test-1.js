import React from 'react'
import styled from 'styled-components'

import {SectionDocument} from './section--document'
import {SectionFieldsEditor} from './section--fields-editor'
import {SectionToolbar} from './section--toolbar'
import {SectionUpload} from './section--upload'
import {SectionJson} from './section--json'

const A = {}

export function ScreenPdfFormBuilderTest1() {
  return (
    <A.Screen id="pdf">
      <SectionDocument />
      <SectionFieldsEditor />
      <SectionToolbar />
      <SectionUpload />
      <SectionJson />
    </A.Screen>
  )
}

A.Screen = styled.div`
  display: grid;
`
