import {useStore} from 'effector-react'
import React from 'react'
import {Rnd} from 'react-rnd'
import styled from 'styled-components'

import {fromFields} from './domain--fields'
import {fromEditor} from './domain--editor'
import SignatureCanvas from 'react-signature-canvas'

const A = {}

function FieldInput() {
  const [value, setValue] = React.useState(``)

  function onChange({target: {value}}) {
    setValue(value)
  }

  return <input value={value} onChange={onChange} />
}

function FieldSign({doc}) {
  return (
    <SignatureCanvas
      penColor="black"
      backgroundColor="rgba(255, 255, 255, 1)"
      canvasProps={{
        width: doc.width,
        height: doc.height,
      }}
    />
  )
}

const typesMap = {
  [fromFields.util.types.input]: FieldInput,
  [fromFields.util.types.sign]: FieldSign,
}

function Field({doc, onMove, onResize, isPreview}) {
  const enableResizing = isPreview ? {} : {bottomRight: true}

  const B = typesMap[doc.type]

  return (
    <A.Card
      disableDragging={isPreview}
      enableResizing={enableResizing}
      default={{
        x: 0,
        y: 0,
        width: 320,
        height: 200,
      }}
      onDragStop={onMove}
      onResizeStop={onResize}
      isPreview={isPreview}
    >
      {isPreview ? <B doc={doc} /> : `${doc.type} :: Move and resize me`}
    </A.Card>
  )
}

const on = {}
on.move = fromFields.on.update.prepend(({id, e, d}) => ({id, x: d.x, y: d.y}))
on.resize = fromFields.on.update.prepend(({id, e, direction, ref}) => ({
  id,
  width: ref.style.width,
  height: ref.style.height,
}))

export function SectionFieldsEditor() {
  const data = useStore(fromFields.$.data)
  const order = useStore(fromFields.$.order)
  const isPreview = useStore(fromEditor.$.isPreview)

  return order.map((id) => (
    <Field
      key={id}
      doc={data[id]}
      onMove={(e, d) => on.move({id, e, d})}
      onResize={(e, direction, ref) => on.resize({id, e, direction, ref})}
      isPreview={isPreview}
    />
  ))
}

const cardStyle = ({isPreview}) =>
  isPreview
    ? ``
    : `
  border: solid 1px #ddd;
  background: #fefef0;
`

A.Card = styled(Rnd)`
  display: flex !important;
  align-items: center;
  justify-content: center;
  ${cardStyle}

  & input {
    width: 100%;
    height: 100%;
    border: none;
  }
`
