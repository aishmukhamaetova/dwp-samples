import {createDomain} from 'effector'
import {restore} from 'effector'

import {fromFields} from './domain--fields'

const who = [`fields`]

const domain = createDomain(who.join(`.`))

const debug = (...args) => console.log(...who, ...args)

const $ = {}
const on = {}
const run = {}

on.toggle = domain.event()
$.isPreview = domain.store(false)
$.isPreview.on(on.toggle, (v) => !v)

on.file = domain.event()
$.file = restore(on.file, null)
$.file.watch((v) => debug(`$.file`, v))

run.add = domain.effect({
  handler() {
    fromFields.on.upsert(fromFields.util.makeOne())
  },
})

run.add = domain.effect({
  handler() {
    fromFields.on.upsert(fromFields.util.makeOne())
  },
})

run.addSign = domain.effect({
  handler() {
    fromFields.on.upsert(
      fromFields.util.makeOne({type: fromFields.util.types.sign}),
    )
  },
})

export const fromEditor = {
  $,
  on,
  run,
}
