import React from 'react'
import FormBuilder from 'react-form-builder2'

import 'react-form-builder2/dist/app.css'

const store = FormBuilder.ElementStore

store.subscribe((state) => console.log(`STATE`, state.data))

export function ScreenFormBuilderTest1() {
  const [text, setText] = React.useState(``)

  React.useEffect(() => {
    let is = true

    store.subscribe(
      (state) => is && setText(JSON.stringify(state.data, null, 2)),
    )

    return () => (is = false)
  })

  return (
    <>
      <FormBuilder.ReactFormBuilder />
      <pre>{text}</pre>
    </>
  )
}
