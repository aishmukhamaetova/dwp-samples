# README

## Applications

- @dwp/admin
- @dwp/ws
- @dwp/wsff

## Packages

- @dwp/admin--domains - Admin frontend state domains with effector
- @dwp/admin--screens - Admin frontend screens

- @dwp/ws--domains - WS frontend state domains with effector
- @dwp/ws--screens - WS frontend screens

- @dwp/wsff--domains - WSFF frontend state domains with effector
- @dwp/wsff--screens - WSFF frontend screens

— @dwp/ui - Common UI components

- @dwp/frontend - Repository for POC tasks (Prove of concept / test some functionality)

## Frontend applications structure

```
app
  Route
    Page
      Screen
        Section
          Block
            Block
            Component
```

- Page - router connector to screen.
- Screen - page without knowing anything about router.
- Section - usual vertical part of page with domain connection.
- Block - logical block of section with domain connection.
- Component - any reusable pure component.

## Development

### Install dependencies

```
npx lerna bootstrap

```

### RUN watcher

```
npx preconstruct watch
```

### Run frontend test application

_Watcher required._

```
yarn --cwd app/frontend start
```

### RUN admin application

_Watcher required._

```
yarn --cwd app/admin start
```

### RUN ws application

_Watcher required._

```
yarn --cwd app/ws start
```

### RUN wsff application

_Watcher required._

```
yarn --cwd app/wsff start
``
```

### BUILD app/frontend APP for SCF BRS subaccount

Source https://developers.sap.com/tutorials/cp-cf-download-cli.html,
https://help.sap.com/viewer/65de2977205c403bbc107264b8eccf4b/Cloud/en-US/d04fc0e2ad894545aebfd7126384307c.html

```
cf login -a https://api.cf.ap10.hana.ondemand.com
#API endpoint:   https://api.cf.ap10.hana.ondemand.com (API version: 3.81.0)
#add your credentials
#choose space <Targeted space Digital Workpacks>

cf apps
#dwp--frontend   started           1/1         128M     1G     dwp--frontend.cfapps.ap10.hana.ondemand.com
#for redeploy remove app first
cf delete dwp--frontend

#prep packages
npx lerna bootstrap

#deploy test app
yarn --cwd app/frontend build
cd app/frontend
#be sure manifest is exist in the folder
#pwd
#/Users/miukki/Projcets/digital-workpacks/app/frontend
ls
manifest.yml #here
#before push to Sap Cloud Foundry please remove @dwp workspaces in /app/frontend/package.json but not push (as we need it for `yarn --cwd app/frontend build`)
#-    "@dwp/domains": "^0.0.1",
#-    "@dwp/screens": "^0.0.1",
#push to Sap Cloud Foundry
cf push
#done
https://dwp--frontend.cfapps.ap10.hana.ondemand.com/

#check the logs for dwp--frontend
cf logs dwp--frontend --recent

#stream application logs
cf logs myapp | grep -v RTR

#use specific nodejs-buildpack, example:
cf push myapp -b https://github.com/cloudfoundry/nodejs-buildpack#develop



```

# Backend packages and run guide

```
@dwp/express
@dwp/db
@dwp/km
```

# KM integration

```
KM is Docoment respository provided by Broadspectrum
```
