using { Currency, managed, sap, cuid } from '@sap/cds/common';
namespace sap.capire.backend;


entity Contracts : managed, cuid {
  name : localized String(128);
  contractLocations : Composition of many ContractLocations on contractLocations.contract = $self;
}

entity Locations : managed, cuid {
  name : localized String(128);
  contractLocations : Association to many ContractLocations on contractLocations.location = $self;
}

entity ContractLocations : managed, cuid {
  contract : Association to Contracts;
  location : Association to Locations;
  locationSuperUsers : Composition of many LocationSuperUsers on locationSuperUsers.contractLocation = $self;
}

entity LocationSuperUsers : managed, cuid {
  userIASId : String(20);
  contractLocation : Association to ContractLocations;
}

entity LocationTeams : managed, cuid {
  contract : Association to Contracts;
  location : Association to Locations;
  name : localized String(128);
  userLocationTeams : Association to many UserLocationTeams on userLocationTeams.locationTeam = $self;
  dwpTemplateLocationTeam : Association to many DWPTemplateLocationTeam on dwpTemplateLocationTeam.locationTeam = $self;
}

entity UserLocationTeams : managed, cuid {
  contract : Association to Contracts;
  location : Association to Locations;
  locationTeam : Association to LocationTeams;
  role: Association to Roles;
  name : localized String(128);
}

entity Roles : managed, cuid {
  userLocationTeams : Association to many UserLocationTeams on userLocationTeams.role = $self;
  name : localized String(128);
  description : localized String(128);
  rolePermission : Association to many RolePermissions on rolePermission.role = $self;
}

entity RolePermissions : managed, cuid {
  role : Association to Roles;
  permission : Association to Permissions;
}

entity Permissions : managed, cuid {
  name : localized String(128);
  rolePermission : Association to many RolePermissions on rolePermission.permission = $self;
}

entity DWPTemplate : managed, cuid {
  tag : String(128);
  title : localized String(128);
  status : String(20);
  dwpTemplateLocationTeam : Association to many DWPTemplateLocationTeam on dwpTemplateLocationTeam.dwpTemplate = $self;
  dwpTemplateDWPTemplateCategory : Association to many DWPTemplateDWPTemplateCategory on dwpTemplateDWPTemplateCategory.dwpTemplate = $self;
  dwpTemplateFormSection : Association to many DWPTemplateFormSection on dwpTemplateFormSection.dwpTemplate = $self;
  dwpTemplateInitialDWP : Association to many DWPTemplateInitialDWP on dwpTemplateInitialDWP.dwpTemplate = $self;
}

entity DWPTemplateLocationTeam : managed, cuid {
  dwpTemplate : Association to DWPTemplate;
  locationTeam : Association to LocationTeams;
}

entity DWPTemplateCategory : managed, cuid {
  dwpTemplate : Association to DWPTemplate;
  name : localized String(128);
  dwpTemplateDWPTemplateCategory : Association to many DWPTemplateDWPTemplateCategory on dwpTemplateDWPTemplateCategory.dwpTemplateCategory = $self;
}

entity DWPTemplateDWPTemplateCategory : managed, cuid {
  dwpTemplate : Association to DWPTemplate;
  dwpTemplateCategory : Association to DWPTemplateCategory;
}

entity FormSectionDWPTemplate : managed, cuid {
  ITP : Boolean;
  FIC : Boolean;
  FORM : Boolean;
  name : localized String(128);
  schemaData : LargeString;
  sectionType : Integer;
  dwpTemplateFormSection : Association to many DWPTemplateFormSection on dwpTemplateFormSection.formSectionDWPTemplate = $self;
  initialDWPHistory : Association to many InitialDWPHistory on initialDWPHistory.formSectionDWPTemplate = $self;
}

entity DWPTemplateFormSection : managed, cuid {
  dwpTemplate : Association to DWPTemplate;
  formSectionDWPTemplate : Association to FormSectionDWPTemplate;
}

entity InitialDWP : managed, cuid {
  projectName : localized String(128);
  title : localized String(128);
  description : localized String(256);
  orderNo : String(30);
  unitNo : String(30);
  equipmentNo : String(30);
  drawingNo : String(30);
  readiness : Decimal(3,2);
  progressStatus : Integer;
  notes : localized String(128);
  initialDWPLocationTeam : Association to many InitialDWPLocationTeam on initialDWPLocationTeam.initialDWP = $self;
  dwpTemplateInitialDWP : Association to many DWPTemplateInitialDWP on dwpTemplateInitialDWP.initialDWP = $self;
  initialDWPHistory : Association to many InitialDWPHistory on initialDWPHistory.initialDWP = $self;
  formSectionInitialDWP : Association to many FormSectionInitialDWP on formSectionInitialDWP.initialDWP = $self;
  initialDWPArchive : Association to many InitialDWPArchive on initialDWPArchive.initialDWP = $self;
}

entity InitialDWPLocationTeam : managed, cuid {
  initialDWP : Association to InitialDWP;
  locationTeam: Association to LocationTeams;
}

entity DWPTemplateInitialDWP : managed, cuid {
  initialDWP : Association to InitialDWP;
  dwpTemplate : Association to DWPTemplate;
}

entity InitialDWPHistory : managed, cuid {
  action : Integer;
  entityType : Integer;
  initialDWP : Association to InitialDWP;
  formSectionDWPTemplate : Association to FormSectionDWPTemplate;
  formSectionInitialDWP : Association to FormSectionInitialDWP;
  initialDWPArchive : Association to InitialDWPArchive;
}

entity FormSectionInitialDWP : managed, cuid {
  initialDWP : Association to InitialDWP;
  userId : String(30);
  status : Integer;
  schemaData : LargeString;
  formData : LargeString;
  initialDWPHistory : Association to many InitialDWPHistory on initialDWPHistory.formSectionInitialDWP = $self;
}

entity InitialDWPArchive : managed, cuid {
  initialDWP : Association to InitialDWP;
  filePathPdf : String(256);
  status : Integer;
  schemaData : LargeString;
  formData : LargeString;
  initialDWPHistory : Association to many InitialDWPHistory on initialDWPHistory.initialDWPArchive = $self;
}

// Use for test JSON serialize / deserialize
entity ContactEmails {
    key ID : Integer;
    kind : String;
    address : String;
    contacts : Association to Contacts;
}

entity Contacts : cuid {
  name : String;
  emails : LargeString;
  contactEmails : Composition of many ContactEmails on contactEmails.contacts = $self;
}
//----------------------------------------------