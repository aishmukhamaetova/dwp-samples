
# Getting Started

Welcome to your new project.

It contains these folders and files, following our recommended project layout:

| File / Folder  | Purpose                              |
| -------------- | ------------------------------------ |
| `db/`          | your domain models and data go here  |
| `srv/`         | your service models and code go here |
| `package.json` | backend metadata and configuration   |
| `readme.md`    | this getting started guide           |

## Deploy HDI-shared container
### 1. open BAS-terminal (https://brstoolscf.ap10cf.applicationstudio.cloud.sap/index.html#ws-vt2kn)
### 2. 
```
cf login 
```
### 3. check compatible versions (and in pakcage.json compatible packages as well)
```

#(to install nvm you can use )
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash

#user: backend $ nvm current
v10.20.1
user: backend $ npx --version
6.14.4
#instsall global
npm install node-gyp@^7.0.0 --global
npm install @sap/hdi-deploy@^3.11.11 --save-dev
```
### 4. Compile and deploy
```
yarn compile (or npm run compile)
yarn deploy (or npm run compile)
```

### 5. Update Package-lock 
```
git add package-lock.json -f
```

## Deploy multiple app

```
cds add mta (only do this when there is no mta.yaml file)
mbt build
cf deploy mta_archives/dwp.backend_1.0.0.mtar
```

## Watcher for Hana DB

- Open a new terminal and run `cds watch`
- ( in VSCode simply choose _**Terminal** > Run Task > cds watch_ )
- Start adding content, e.g. a [db/schema.cds](db/schema.cds), ...

## Setup CDS

https://cap.cloud.sap/docs/cds/
This is the steps to set up in vs code:

---

Set the NPM registry for @sap packages:
npm set @sap:registry=https://npm.sap.com
Install the cds development kit globally:
npm i -g @sap/cds-dk
Go to https://tools.hana.ondemand.com/#hanatools
Search CDS Language Support for Visual Studio Code and download vscode-cds-2.2.0.vsix
Install it in Visual Studio Code Extensions

---

## Learn more...

Learn more at https://cap.cloud.sap/docs/get-started/
