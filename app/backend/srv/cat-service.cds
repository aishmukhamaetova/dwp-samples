using { sap.capire.backend as my } from '../db/schema';
service CatalogService @(path:'/browse') {
  @requires_: 'authenticated-user'
  entity Contacts as projection on my.Contacts;
}