using { sap.capire.backend as my } from '../db/schema';
service AdminService @(_requires:'admin') {
  entity Contracts as projection on my.Contracts;
  entity Locations as projection on my.Locations;
  entity ContractLocations as projection on my.ContractLocations;
  entity LocationSuperUsers as projection on my.LocationSuperUsers;
}